class AddLinkTitleDescription < ActiveRecord::Migration[6.0]
  def change
    add_column :articles, :link_description, :string
  end
end
