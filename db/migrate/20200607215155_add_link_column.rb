class AddLinkColumn < ActiveRecord::Migration[6.0]
  def change
    add_column :articles, :url_link, :string
  end
end
